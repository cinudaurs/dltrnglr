  'use strict';

angular.module('confusionApp')

        .controller('TwitterController', ['$scope', 'twitterFactory', function($scope, twitterFactory) {
            
           

        }])

        
        .controller('FeedbackController', ['$scope', 'feedbackFactory', function($scope, feedbackFactory) {
            
            $scope.feedback = {firstname:"", lastname:"", telcode:"", telnum:"", emailid:"", contactyesorno:"", modeofcontact:"", feedback:"" };
           
            $scope.sendFeedback = function() {
                
                    feedbackFactory.getFeedback().save($scope.feedback);
                    console.log($scope.feedback);
                    $scope.feedbackForm.$setPristine();
                    $scope.feedback = {firstname:"", lastname:"", telcode:"", telnum:"", emailid:"", contactyesorno:"", modeofcontact:"", feedback:"" };
                    
                };
            }
            
        ])

         // implement the IndexController and About Controller here
        .controller('IndexController', ['$scope', 'menuFactory', 'corporateFactory', function($scope, menuFactory, corporateFactory){
            
            
        }])

        
        .controller('AboutController', ['$scope', 'corporateFactory', function($scope, corporateFactory){
            
               
            $scope.makers = corporateFactory.getMakers().query(
            function(response) {
                    
                        $scope.makers = response;
                        $scope.showMakers = true;
                        
                }, 
            function(response){
                $scope.message = "Error: "+response.status + " " + response.statusText;
            }
            );
            
            
        }])


;
